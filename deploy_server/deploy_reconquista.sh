#!/bin/bash

# Pulling docker image
docker pull sayri/mongo:latest
docker pull sayri/extractor:latest
docker pull sayri/collector:latest

docker stack deploy -c docker-compose-reconquista.yml collector
