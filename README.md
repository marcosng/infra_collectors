###Infra Data Collector

Colectores de datos para Sayri de Infra (reconquista.claro.amx).


#### Despliegue local
1. Clonar el repositorio.
2. Ejecutar **./deploy_local_stack.sh** para crear las imágenes Docker y desplegar un *stack* con los 3 contenedores.
3. Corroborar que se haya desplegado ingresando a http://127.0.0.1:5000/


#### Despliegue remoto
1. Copiar la carpeta **deploy_server/** al servidor.
2. Ejecutar **./deploy_reconquista.sh** para descargar las imagenes subidas a Dockerhub y desplegar el stack.


#### Crear y subir imagenes a Dockerhub
1. En caso que no tengamos las imagenes creadas de manera local, ejecutar las 3 primeras lineas del archivo **deploy_local_stack.sh**
2. Tagear y subir las imagenes a Dockerhub mediante la ejecucion del archivo **upload_images.sh**.


#### Imágenes utilizadas
* Base de datos Mongo: **sayri/mongo:latest** (basada en mongo 4.0)
* Servidor web para extraccion de datos: imagen propia a partir de **tiangolo/uwsgi-nginx-flask:python3.6**
* SO ejecucion de script de Python: imagen propia a partir de **python:3.6.7-slim-stretch**


#### Comandos útiles

* Listar imágenes/contenedores/servicios/stacks:  
`$ docker ps -a`  
`$ docker image ls`  
`$ docker service ls`  
`$ docker stack ls`

* Eliminar contenedores/imagenes/stacks:  
`$ docker rm -f $(docker ps -a -q)`  
`$ docker rmi $(docker image ls -q)`  
`$ docker stack rm <stack_name>` 

* Ejecutar *bash* dentro de un contenedor:  
`$ docker exec -it <container_id> bash`

* Crear un contenedor a partir de una imagen, lanzarlo y ejecutar *bash*:  
`$ docker run -it <image_id> bash`

* Imprimir donde se guardan los volúmenes locales del stack:  
`$ docker volume inspect <stack_name>_<volume>`

* Crear tag de imagen:  
`$ docker tag <image_name> <tag_name>`

* Subir imagen a docker-hub:  
`$ docker login --username <user> --password <password>`  
`$ docker push <tag_name>`