#!/usr/bin/env bash
USERNAME=sayri
PASSWORD=c3r4g0nadmin

# Login in Dockerhub
docker login --username $USERNAME --password $PASSWORD

# Tagging images
docker tag collector:latest $USERNAME/collector:latest
docker tag extractor:latest $USERNAME/extractor:latest

# Pushing images to Dockerhub
docker push $USERNAME/collector:latest
docker push $USERNAME/extractor:latest