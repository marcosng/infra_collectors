import time
from database import MongoDatabaseManager
from input import get_ups, get_powerplants, get_generators, get_commercial_energy
from collectors import powerplant_ge, powerplant_eaton, powerplant_breakers, powerplant_eltek, powerplant_emerson
from collectors import ups_eaton, ups_sch_galaxy, ups_sch_symmetra
from collectors import commercial_energy_ion, commercial_energy_sch_pm800
from collectors import comap, deif


# COLECTOR
def powerplants_collector(source_file, db_object):
    """
    Reads the file with the PowerPlant information.
    Stores collected data in 'result' list.
    Writes database
    :param source_file: string. Path to the file with PowerPlants information
    :param db_object: MongoDatabaseManager object.
    :return:
    """
    # Gets PowerPlant names, IP and brand
    pp_names, pp_ips, pp_brands, pp_bmon = get_powerplants(source_file)

    for i in range(len(pp_names)):

        if 'eaton' in pp_brands[i].lower():
            result = powerplant_eaton(pp_ips[i], pp_names[i])

        elif 'general electric' in pp_brands[i].lower():
            result = powerplant_ge(pp_ips[i], pp_names[i])

        elif 'eltek' in pp_brands[i].lower():
            result = powerplant_eltek(pp_ips[i], pp_names[i])

        elif 'emerson' in pp_brands[i].lower():
            result = powerplant_emerson(pp_ips[i], pp_names[i])

        else:
            result = None

        if result is not None:
            # Writes in database
            db_object.insert_data('powerplant', result)
            #print(result)
    #print()


def ups_collector(source_file, db_object):
    """
    Reads the file with the UPS information.
    Stores collected data in ' list.
    Writes database
    :param source_file: string. Path to the file with PowerPlants information
    :param db_object: MongoDatabaseManager object.
    :return:
    """
    ups_names, ups_brands, ups_models, ups_ips = get_ups(source_file)

    for i in range(len(ups_names)):

        result = None
        if 'schneider' in ups_brands[i].lower():
            if 'symmetra' in ups_models[i].lower():
                result = ups_sch_symmetra(ups_ips[i], ups_names[i])
            elif 'galaxy' in ups_models[i].lower():
                result = ups_sch_galaxy(ups_ips[i], ups_names[i])

        elif 'eaton' in ups_brands[i].lower():
            result = ups_eaton(ups_ips[i], ups_names[i])

        if result is not None:
            # Writes in database
            db_object.insert_data('ups', result)
            #print(result)


def generators_collector(source_file, db_object):
    """
    Reads the file with the Generators information.
    Stores collected data in dictionary.
    Writes database
    :param source_file: string. Path to the file with Generators information
    :param db_object: MongoDatabaseManager object.
    :return:
    """
    gen_names, gen_contr, gen_ips = get_generators(source_file)

    for i in range(len(gen_names)):

        result = None
        if gen_contr[i] == 'DEIF':
            result = deif(gen_ips[i], gen_names[i], type='gen')
        elif gen_contr[i] == 'ComAp':
            result = comap(gen_ips[i], gen_names[i], type='gen')

        if result is not None:
            if list(result.values()).count(0) < 7:
                # Writes in database if the collection retrieved data and data is not null
                db_object.insert_data('generator', result)
                #print(result)
    #print()


def commercial_energies_collector(source_file, db_object):
    """
    Reads the file with the Commercial Energy information.
    Calls the proper collectors functions acording to the type: "DEIF" / "ION"
    Saves the collected data into the database
    :param source_file: string. Path to the file with Generators information
    :param db_object: MongoDatabaseManager object.
    :return:
    """
    ce_names, ce_contr, ce_ips = get_commercial_energy(source_file)

    for i in range(len(ce_names)):

        result = None
        if ce_contr[i] == 'DEIF':
            result = deif(ce_ips[i], ce_names[i], type='ce')
        elif ce_contr[i] == 'ION':
            result = commercial_energy_ion(ce_ips[i], ce_names[i])
        elif ce_contr[i] == 'ComAp':
            result = comap(ce_ips[i], ce_names[i], type='ce')
        elif ce_contr[i] == 'PM800':
            result = commercial_energy_sch_pm800(ce_ips[i], ce_names[i])
        if result is not None:
            db_object.insert_data('commercial_energy', result)
            #print(result)
    #print()


def powerplant_breakers_collector(source_file, db_object):
    """
    Reads the file with the PowerPlant information.
    Stores collected data in 'result' list.
    Writes database
    :param source_file: string. Path to the file with PowerPlants information
    :param db_object: MongoDatabaseManager object.
    :return:
    """
    # Gets PowerPlant names, IP and brand
    pp_names, pp_ips, pp_brands, pp_bmon = get_powerplants(source_file)

    j = 0
    for i in range(len(pp_names)):
        if pp_bmon[i] == 'True':
            j += 1
            result = powerplant_breakers(pp_ips[i], pp_names[i])
        else:
            result = None

        if result is not None:
            # Writes in database
            db_object.insert_many_data('powerplant_breakers', result)
            #print(result)

    #print()


if __name__ == '__main__':
    # Database name and source file
    DATABASE_NAME = 'collector'
    SOURCE_FILE = "/app/Monitoreo_Sitios_v2.xlsx"

    database = MongoDatabaseManager(DATABASE_NAME)

    print("START:", time.strftime('%H:%M:%S'))
    commercial_energies_collector(SOURCE_FILE, database)
    generators_collector(SOURCE_FILE, database)
    powerplants_collector(SOURCE_FILE, database)
    ups_collector(SOURCE_FILE, database)
    powerplant_breakers_collector(SOURCE_FILE, database)
    print("END:", time.strftime('%H:%M:%S'))
