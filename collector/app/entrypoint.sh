#!/bin/bash

set -e

sleep 10s

echo -n "Creado base de datos..."
mongo --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.getSiblingDB('collector')" > /dev/null
sleep 2s
echo "OK!"

echo -n "Creando colecciones..."
mongo collector --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.createCollection('powerplant')" > /dev/null
mongo collector --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.createCollection('ups')" > /dev/null
mongo collector --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.createCollection('commercial_energy')" > /dev/null
mongo collector --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.createCollection('generator')" > /dev/null
mongo collector --host mongodb --username mongo --password mongo --authenticationDatabase admin --eval "db.createCollection('powerplant_breakers')" > /dev/null
sleep 2s
echo "OK!"

echo -n "Iniciando programador de tareas..."
service cron restart
echo "OK!"

echo -n "Agregando tarea programada..."
chmod +x /app/start.sh
crontab /app/start.sh
service cron reload
echo "OK!"

exec "$@"
