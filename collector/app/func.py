import re
import platform
import subprocess

from pyModbusTCP.client import ModbusClient
from pysnmp.hlapi import *


# UTILS
def consulta_snmp(ip_address, oid, snmp_ver=0, community='public'):
    """
    Gets data from a NE through SNMP protocol.
    :param ip_address: string. NEs IP address
    :param oid: string. NEs Object Identifier (OID)
    :param snmp_ver: int. SNMP version
    :param community: string. Authentication
    :return: string/int/float. Data retrieved
    """
    g = getCmd(SnmpEngine(),
               CommunityData(community, mpModel=snmp_ver),
               UdpTransportTarget((ip_address, 161)),
               ContextData(),
               ObjectType(ObjectIdentity(oid))
               )
    errorIndication, errorStatus, errorIndex, varBinds = next(g)
    return varBinds[0]


def consulta_modbustcp(ip_address, address_bit, number_of_bits=1):
    """
    Gets data from a NE through ModBusTCP protocol.
    :param ip_address: string. NEs IP address
    :param address_bit: int. Data address
    :param number_of_bits: default=1
    :return: string/int/float. Data retrieved
    """
    cliente = ModbusClient(host=ip_address, auto_open=True, auto_close=True)
    valor = cliente.read_input_registers(address_bit, number_of_bits)
    # print('Registro:',address_bit, '\t\tDato:',valor)
    return valor[0]


def snmp_walk(ip_address, oid, snmp_ver=0, community='public', ):
    """
    Performs a SNMPWalk to a given IP address
    :param ip_address: string. IP address
    :param oid: string. OID
    :param output: list.
    :param snmp_ver: int. SNMP version 0=v1, 1=v2, 2=v3.
    :param community: string. Community string.
    :return:
    """
    output = []
    for (errorIndication,
         errorStatus,
         errorIndex,
         varBinds) in nextCmd(SnmpEngine(),
                              CommunityData(community, mpModel=snmp_ver),
                              UdpTransportTarget((ip_address, 161)),
                              ContextData(),
                              ObjectType(ObjectIdentity(oid)),
                              lexicographicMode=False):

        val = varBinds[0][1].prettyPrint()
        if val.isdigit():
            val = int(val)
        else:
            val = str(val)
        output.append(val)
    return output


def check_connectivity(ip):
    """
    Checks device SNMP / ModBusTCP connectivity
    :param ip: string. IP address
    :return: string. OK / NOK
    """
    try:
        sysName = '1.3.6.1.2.1.1.5.0'
        consulta_snmp(ip, sysName)[1].prettyPrint()
        print(ip, "OK")
        return 'OK'
    except:
        pass

    try:
        sysName = '1.3.6.1.2.1.1.5.0'
        consulta_snmp(ip, sysName, snmp_ver=1)[1].prettyPrint()
        print(ip, "OK")
        return 'OK'
    except:
        pass

    try:
        consulta_modbustcp(ip, 0)
        print(ip, "OK")
        return 'OK'
    except:
        pass

    print(ip, "NOK")
    return 'NOK'