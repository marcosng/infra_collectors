import time
from func import consulta_modbustcp, consulta_snmp, snmp_walk, check_connectivity


# COLUMNS
POWERPLANT_NAME = 'Planta de energía cc'
DATE = 'Fecha'
IP_ADDRESS = 'Dirección IP de adminitración'
OUTPUT_POWER = 'Potencia de salida instalada medida (kW)'
OUTPUT_VOLTAGE = 'Voltaje de salida medido (V)'
RECTIFIER_OUTPUT_CURRENT = 'Corriente de salida del rectificador medida (A)'
BATTERY_OUTPUT_CURRENT = 'Corriente de salida medida de batería (A)'
LOAD_OUTPUT_CURRENT = 'Corriente de salida de carga medida (A)'
RECTIFIER_OUTPUT_POWER = 'Potencia de salida del rectificador medida (kW)'
BATTERY_OUTPUT_POWER = 'Potencia de salida de baterías medida (kW)'
LOAD_OUTPUT_POWER = 'Potencia de salida de carga medida (kW)'
IGNORED = 'Ignorado'

UPS_NAME = 'Ups'
OUTPUT_VOLTAGE_L1 = 'Voltaje de salida medido en línea 1 (V)'
OUTPUT_VOLTAGE_L2 = 'Voltaje de salida medido en línea 2 (V)'
OUTPUT_VOLTAGE_L3 = 'Voltaje de salida medido en línea 3 (V)'
OUTPUT_CURRENT_L1 = 'Corriente de salida medida en línea 1 (A)'
OUTPUT_CURRENT_L2 = 'Corriente de salida medida en línea 2 (A)'
OUTPUT_CURRENT_L3 = 'Corriente de salida medida en línea 3 (A)'
REAL_OUTPUT_POWER = 'Potencia real de salida (kW)'
APPARENT_OUTPUT_POWER = 'Potencia aparente de salida (KVA)'
POWER_FACTOR = 'Factor de potencia'

GENERATOR_NAME = 'Generador'

COMMERCIAL_ENERGY_NAME = 'Energía comercial'
PEAK_POWER = 'Pico de potencia medido (kW)'

BREAKER_NAME = 'Interruptor'
MEASURED_CURRENT = 'Corriente medida (A)'
CALIBRATED_CURRENT = 'Corriente calibrada (A)'
ASIGNED_CURRENT = 'Corriente asignada (A)'
DESCRIPTION = 'Descripcion'


# UPSs
def ups_eaton(ip, device_name):
    """
    EATON UPS SNMP collector
    :param ip: string. UPS IP address
    :param device_name: string. UPS name
    :return: dic/None. ups_data
    """
    if check_connectivity(ip) == 'NOK':
        ups_data = None

    else:
        # EATON UPS OIDs
        # upsOutputVoltage
        Ov1 = '1.3.6.1.2.1.33.1.4.4.1.2.1'
        Ov2 = '1.3.6.1.2.1.33.1.4.4.1.2.2'
        Ov3 = '1.3.6.1.2.1.33.1.4.4.1.2.3'

        # upsOutputCurrent
        Oc1 = '1.3.6.1.2.1.33.1.4.4.1.3.1'
        Oc2 = '1.3.6.1.2.1.33.1.4.4.1.3.2'
        Oc3 = '1.3.6.1.2.1.33.1.4.4.1.3.3'

        # upsOutputPower
        Orp1 = '1.3.6.1.2.1.33.1.4.4.1.4.1'
        Orp2 = '1.3.6.1.2.1.33.1.4.4.1.4.2'
        Orp3 = '1.3.6.1.2.1.33.1.4.4.1.4.3'

        # upsConfigOutputVA / upsConfigOutputPower
        Orp = '1.3.6.1.2.1.33.1.9.6.0'
        Oap = '1.3.6.1.2.1.33.1.9.5.0'

        ups_data = {UPS_NAME: device_name,
                    DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                    IP_ADDRESS: ip,
                    IGNORED: 0}

        # VOLTAGES
        try:
            ups_data[OUTPUT_VOLTAGE_L1] = int(consulta_snmp(ip, Ov1)[1])
        except:
            pass

        try:
            ups_data[OUTPUT_VOLTAGE_L2] = int(consulta_snmp(ip, Ov2)[1])
        except:
            pass

        try:
            ups_data[OUTPUT_VOLTAGE_L3] = int(consulta_snmp(ip, Ov3)[1])
        except:
            pass

        # CURRENTS
        try:
            ups_data[OUTPUT_CURRENT_L1] = round(float(consulta_snmp(ip, Oc1)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L2] = round(float(consulta_snmp(ip, Oc2)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L3] = round(float(consulta_snmp(ip, Oc3)[1] / 10), 2)
        except:
            pass

        # REAL POWER
        try:
            pr1 = float(consulta_snmp(ip, Orp1)[1] / 1000)
        except:
            pass

        try:
            pr2 = float(consulta_snmp(ip, Orp2)[1] / 1000)
        except:
            pass

        try:
            pr3 = float(consulta_snmp(ip, Orp3)[1] / 1000)
        except:
            pass

        try:
            ups_data[REAL_OUTPUT_POWER] = round(pr1 + pr2 + pr3, 2)
        except:
            pass

        try:
            ups_data[OUTPUT_POWER] = round(float(consulta_snmp(ip, Orp)[1] / 1000), 2)
        except:
            pass

        # APPARENT POWER
        try:
            pa1 = float(ups_data[OUTPUT_VOLTAGE_L1] * ups_data[OUTPUT_CURRENT_L1] / 1000)
        except:
            pass

        try:
            pa2 = float(ups_data[OUTPUT_VOLTAGE_L2] * ups_data[OUTPUT_CURRENT_L2] / 1000)
        except:
            pass

        try:
            pa3 = float(ups_data[OUTPUT_VOLTAGE_L3] * ups_data[OUTPUT_CURRENT_L3] / 1000)
        except:
            pass

        try:
            ups_data[APPARENT_OUTPUT_POWER] = round(pa1 + pa2 + pa3, 2)
        except:
            pass

        # POWER FACTOR
        try:
            ups_data[POWER_FACTOR] = round(float(ups_data[REAL_OUTPUT_POWER] / ups_data[APPARENT_OUTPUT_POWER]), 2)
        except:
            pass

    return ups_data


def ups_sch_symmetra(ip, device_name):
    """
    SCHNEIDER SYMMETRA UPS SNMP collector
    :param ip: string. UPS IP address
    :param device_name: string. UPS name
    :return: dic/None. ups_data
    """
    if check_connectivity(ip) == 'NOK':
        ups_data = None

    else:
        # SCHNEIDER: APC SYMMETRA PX UPS OIDs
        # upsPhaseOutputVoltage
        Ov1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.3.1.1.1'
        Ov2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.3.1.1.2'
        Ov3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.3.1.1.3'

        # upsPhaseOutputCurrent (0.1 Amp)
        Oc1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.4.1.1.1'
        Oc2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.4.1.1.2'
        Oc3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.4.1.1.3'

        # upsPhaseOutputLoad
        Oap1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.1'
        Oap2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.2'
        Oap3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.3'

        # upsAdvOutputKVACapacity
        Oap = '1.3.6.1.4.1.318.1.1.1.4.2.6.0'

        # upsPhaseOutputPercentPower
        Orpp1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.16.1.1.1'
        Orpp2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.16.1.1.2'
        Orpp3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.16.1.1.3'

        ups_data = {UPS_NAME: device_name,
                    DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                    IP_ADDRESS: ip,
                    IGNORED: 0}

        # VOLTAGES
        try:
            ups_data[OUTPUT_VOLTAGE_L1] = int(consulta_snmp(ip, Ov1)[1])
        except:
            pass

        try:
            ups_data[OUTPUT_VOLTAGE_L2] = int(consulta_snmp(ip, Ov2)[1])
        except:
            pass

        try:
            ups_data[OUTPUT_VOLTAGE_L3] = int(consulta_snmp(ip, Ov3)[1])
        except:
            pass

        # CURRENTS
        try:
            ups_data[OUTPUT_CURRENT_L1] = round(float(consulta_snmp(ip, Oc1)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L2] = round(float(consulta_snmp(ip, Oc2)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L3] = round(float(consulta_snmp(ip, Oc3)[1] / 10), 2)
        except:
            pass

        # REAL POWER (PERCENTAGE & kW)
        try:
            ups_data[OUTPUT_POWER] = int(consulta_snmp(ip, Oap)[1])
        except:
            pass

        try:
            pr1 = int(consulta_snmp(ip, Orpp1)[1])
        except:
            pass

        try:
            pr2 = int(consulta_snmp(ip, Orpp2)[1])
        except:
            pass

        try:
            pr3 = int(consulta_snmp(ip, Orpp3)[1])
        except:
            pass

        try:
            Pr1 = float(ups_data[OUTPUT_POWER] / 3) * float(pr1 / 100)
        except:
            pass

        try:
            Pr2 = float(ups_data[OUTPUT_POWER] / 3) * float(pr2 / 100)
        except:
            pass

        try:
            Pr3 = float(ups_data[OUTPUT_POWER] / 3) * float(pr3 / 100)
        except:
            pass

        try:
            ups_data[REAL_OUTPUT_POWER] = round(Pr1 + Pr2 + Pr3, 2)
        except:
            pass

        # APPARENT POWER
        try:
            pa1 = float(consulta_snmp(ip, Oap1)[1] / 1000)
        except:
            pass

        try:
            pa2 = float(consulta_snmp(ip, Oap2)[1] / 1000)
        except:
            pass

        try:
            pa3 = float(consulta_snmp(ip, Oap3)[1] / 1000)
        except:
            pass

        try:
            ups_data[APPARENT_OUTPUT_POWER] = round(pa1 + pa2 + pa3, 2)
        except:
            pass

        # POWER FACTOR
        try:
            ups_data[POWER_FACTOR] = round(float(ups_data[REAL_OUTPUT_POWER] / ups_data[APPARENT_OUTPUT_POWER]), 2)
        except:
            pass

    return ups_data


def ups_sch_galaxy(ip, device_name):
    """
    SCHNEIDER GALAXY UPS SNMP collector
    :param ip: string. UPS IP address
    :param device_name: string. UPS name
    :return: dic/None. ups_data
    """
    if check_connectivity(ip) == 'NOK':
        ups_data = None

    else:
        # SCHNEIDER GALAXY UPS OIDs
        # upsPhaseOutputCurrent (0.1 Amp)
        Oc1 = '1.3.6.1.2.1.33.1.4.4.1.3.1'
        Oc2 = '1.3.6.1.2.1.33.1.4.4.1.3.2'
        Oc3 = '1.3.6.1.2.1.33.1.4.4.1.3.3'

        # upsOutputPower
        Orp1 = '1.3.6.1.2.1.33.1.4.4.1.4.1'
        Orp2 = '1.3.6.1.2.1.33.1.4.4.1.4.2'
        Orp3 = '1.3.6.1.2.1.33.1.4.4.1.4.3'

        # Apparent Power (VA) -- ?
        Oap1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.1'
        Oap2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.2'
        Oap3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.7.1.1.3'

        # Power Factors(* 100)
        Pf1 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.19.1.1.1'
        Pf2 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.19.1.1.2'
        Pf3 = '1.3.6.1.4.1.318.1.1.1.9.3.3.1.19.1.1.3'

        # Installed Power (kVA)
        p = '1.3.6.1.4.1.318.1.1.1.4.2.6.0'

        ups_data = {UPS_NAME: device_name,
                    DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                    IP_ADDRESS: ip,
                    IGNORED: 0,
                    OUTPUT_VOLTAGE_L1: 220,
                    OUTPUT_VOLTAGE_L2: 220,
                    OUTPUT_VOLTAGE_L3: 220}

        # CURRENTS
        try:
            ups_data[OUTPUT_CURRENT_L1] = round(float(consulta_snmp(ip, Oc1)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L2] = round(float(consulta_snmp(ip, Oc2)[1] / 10), 2)
        except:
            pass

        try:
            ups_data[OUTPUT_CURRENT_L3] = round(float(consulta_snmp(ip, Oc3)[1] / 10), 2)
        except:
            pass

        # REAL POWER
        try:
            pr1 = float(consulta_snmp(ip, Orp1)[1] / 1000)
        except:
            pass

        try:
            pr2 = float(consulta_snmp(ip, Orp2)[1] / 1000)
        except:
            pass

        try:
            pr3 = float(consulta_snmp(ip, Orp3)[1] / 1000)
        except:
            pass

        try:
            ups_data[REAL_OUTPUT_POWER] = round(pr1 + pr2 + pr3, 2)
        except:
            pass

        try:
            ups_data[OUTPUT_POWER] = int(consulta_snmp(ip, p)[1])
        except:
            pass

        # APPARENT POWER
        try:
            pa1 = float(consulta_snmp(ip, Oap1)[1] / 1000)
        except:
            pass

        try:
            pa2 = float(consulta_snmp(ip, Oap2)[1] / 1000)
        except:
            pass

        try:
            pa3 = float(consulta_snmp(ip, Oap3)[1] / 1000)
        except:
            pass

        try:
            ups_data[APPARENT_OUTPUT_POWER] = round(pa1 + pa2 + pa3, 2)
        except:
            pass

        # POWER FACTOR
        # try:
        #     pf1 = round(float(consulta_snmp(ip, Pf1)[1] / 1000), 2)
        # except:
        #     pass
        #
        # try:
        #     pf2 = round(float(consulta_snmp(ip, Pf2)[1] / 1000), 2)
        # except:
        #     pass
        #
        # try:
        #     pf3 = round(float(consulta_snmp(ip, Pf3)[1] / 1000), 2)
        # except:
        #     pass
        #
        # try:
        #     ups_data[POWER_FACTOR] = round((pf1 + pf2 + pf3)/3, 2)
        # except:
        #     pass

        try:
            ups_data[POWER_FACTOR] = round(float(ups_data[REAL_OUTPUT_POWER] / ups_data[APPARENT_OUTPUT_POWER]), 2)
        except:
            pass

    return ups_data


# POWERPLANTs
def powerplant_eaton(ip, device_name):
    """
    EATON PowerPlant SNMP collector
    :param ip: string. Powerplant IP address
    :param device_name: string. Powerplant name
    :return: dic/None. powerplant_data
    """
    if check_connectivity(ip) == 'NOK':
        powerplant_data = None
    else:
        # EATON PowerPlant OIDs
        Vo = "1.3.6.1.4.1.1918.2.13.10.90.10.30.1.40.1"  # Bus voltage (5450 V)
        Ir = "1.3.6.1.4.1.1918.2.13.10.60.20.0"  # Rectifier current (613 A)
        Ic = "1.3.6.1.4.1.1918.2.13.10.50.10.0"  # Load current (601 A)
        Ps = "1.3.6.1.4.1.1918.2.13.10.50.20.0"  # System Power (35 %)
        Rec = "1.3.6.1.4.1.1918.2.13.10.60.10.0"  # Cantidad de rectificadores

        powerplant_data = {POWERPLANT_NAME: device_name,
                           DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                           IP_ADDRESS: ip,
                           IGNORED: 0}

        # VOLTAGE
        try:
            powerplant_data[OUTPUT_VOLTAGE] = round(float(consulta_snmp(ip, Vo)[1] / 100), 2)
        except:
            pass

        # CURRENTS
        try:
            powerplant_data[RECTIFIER_OUTPUT_CURRENT] = round(int(consulta_snmp(ip, Ir)[1]), 2)
        except:
            pass

        try:
            powerplant_data[LOAD_OUTPUT_CURRENT] = round(int(consulta_snmp(ip, Ic)[1]), 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_CURRENT] = powerplant_data[LOAD_OUTPUT_CURRENT] - \
                                                      powerplant_data[RECTIFIER_OUTPUT_CURRENT]
        except:
            pass

        # POWERS
        try:
            powerplant_data[RECTIFIER_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                            powerplant_data[RECTIFIER_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[LOAD_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                       powerplant_data[LOAD_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_POWER] = round(-powerplant_data[OUTPUT_VOLTAGE] *
                                                          powerplant_data[BATTERY_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[OUTPUT_POWER] = round(5.8 * float(consulta_snmp(ip, Rec)[1]), 2)
        except:
            pass

    return powerplant_data


def powerplant_ge(ip, device_name):
    """
    GENERAL ELECTRIC PowerPlant SNMP collector
    :param ip: string. Powerplant IP address
    :param device_name: string. Powerplant name
    :return: dic/None. powerplant_data
    """
    if check_connectivity(ip) == 'NOK':
        powerplant_data = None
    else:
        # GENERAL ELECTRIC PowerPlant OIDs
        Vo = "1.3.6.1.4.1.1751.2.71.1.1.1.4.0"  # Output Voltage (-54000 V)
        Io = "1.3.6.1.4.1.1751.2.71.1.1.1.7.0"  # Output Rectifier Current (3046 A)
        Irc = "1.3.6.1.4.1.1751.2.71.1.1.1.6.0"  # Installed Rectifier Capacity (6160 A)

        powerplant_data = {POWERPLANT_NAME: device_name,
                           DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                           IP_ADDRESS: ip,
                           IGNORED: 0}

        # VOLTAGE
        try:
            powerplant_data[OUTPUT_VOLTAGE] = abs(float(consulta_snmp(ip, Vo, snmp_ver=1, community='public')[1] / 1000))
        except:
            pass

        # CURRENTS
        try:
            powerplant_data[RECTIFIER_OUTPUT_CURRENT] = int(consulta_snmp(ip, Io, snmp_ver=1, community='public')[1])
        except:
            pass

        try:
            Ib = "1.3.6.1.4.1.1751.2.71.1.1.14.15.0"  # GE CONTROLLER
            powerplant_data[BATTERY_OUTPUT_CURRENT] = float(str(consulta_snmp(ip, Ib, snmp_ver=1, community='public')[1])[:-2])
        except:
            try:
                Ib = "1.3.6.1.4.1.1751.2.71.1.1.14.4.0"  # TYCO CONTROLLER
                powerplant_data[BATTERY_OUTPUT_CURRENT] = int(consulta_snmp(ip, Ib, snmp_ver=1, community='public')[1])
            except:
                pass

        try:
            powerplant_data[LOAD_OUTPUT_CURRENT] = powerplant_data[RECTIFIER_OUTPUT_CURRENT] - \
                                                   powerplant_data[BATTERY_OUTPUT_CURRENT]
        except:
            pass

        # POWER
        try:
            powerplant_data[RECTIFIER_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                            powerplant_data[RECTIFIER_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                          powerplant_data[BATTERY_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[LOAD_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                       powerplant_data[LOAD_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            irc = int(consulta_snmp(ip, Irc, snmp_ver=1, community='public')[1])
            powerplant_data[OUTPUT_POWER] = round(54 * irc / 1000, 2)
        except:
            pass

    return powerplant_data


def powerplant_eltek(ip, device_name):
    """
    ELTEK FLATPACK 2 PowerPlant SNMP collector
    :param ip: string. Powerplant IP address
    :param device_name: string. Powerplant name
    :return: dic/None. powerplant_data
    """
    if check_connectivity(ip) == 'NOK':
        powerplant_data = None
    else:
        # GENERAL ELECTRIC PowerPlant OIDs
        Vo = "1.3.6.1.4.1.12148.9.3.2.0"  # Output/Battery Voltage (5354)
        Io = "1.3.6.1.4.1.12148.9.5.3.0"  # Output Rectifier Current (64)
        Irc = "1.3.6.1.4.1.12148.9.5.1.0"  # Installed Rectifiers (6)
        Il = "1.3.6.1.4.1.12148.9.4.1.0" # Load Output Current (63)
        Ib = "1.3.6.1.4.1.12148.9.3.3.0" # Battery Output Current (-3)


        powerplant_data = {POWERPLANT_NAME: device_name,
                           DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                           IP_ADDRESS: ip,
                           IGNORED: 0}

        # VOLTAGE
        try:
            powerplant_data[OUTPUT_VOLTAGE] = abs(float(consulta_snmp(ip, Vo, snmp_ver=1, community='public')[1] / 100))
        except:
            pass

        # CURRENTS
        try:
            powerplant_data[RECTIFIER_OUTPUT_CURRENT] = int(consulta_snmp(ip, Io, snmp_ver=1, community='public')[1])
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_CURRENT] = float(str(consulta_snmp(ip, Ib, snmp_ver=1, community='public')[1]))
        except:
            pass


        try:
            powerplant_data[LOAD_OUTPUT_CURRENT] = float(consulta_snmp(ip, Il, snmp_ver=1, community='public')[1])
        except:
            pass

        # POWER
        try:
            powerplant_data[RECTIFIER_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                            powerplant_data[RECTIFIER_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                          powerplant_data[BATTERY_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[LOAD_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                       powerplant_data[LOAD_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            irc = int(consulta_snmp(ip, Irc, snmp_ver=1, community='public')[1])
            powerplant_data[OUTPUT_POWER] = round(3000 * irc / 1000, 2)
        except:
            pass

    return powerplant_data


def powerplant_emerson(ip, device_name):
    """
    EMERSON NETSURE M830B PowerPlant SNMP collector
    :param ip: string. Powerplant IP address
    :param device_name: string. Powerplant name
    :return: dic/None. powerplant_data
    """
    if check_connectivity(ip) == 'NOK':
        powerplant_data = None
    else:
        # GENERAL ELECTRIC PowerPlant OIDs
        Vo = "1.3.6.1.4.1.6302.2.1.2.2.0"  # Output/Battery Voltage (52874)
        Io = "1.3.6.1.4.1.6302.2.1.2.3.0"  # Output Rectifier Current (89125)
        Irc = "1.3.6.1.4.1.6302.2.1.2.11.1.0"  # Installed Rectifiers (6)
        Il = "1.3.6.1.4.1.6302.2.1.2.12.1.0" # Load Output Current (89903)
        Ib = "1.3.6.1.4.1.6302.2.1.2.5.2.0" # Battery Output Current (0)


        powerplant_data = {POWERPLANT_NAME: device_name,
                           DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                           IP_ADDRESS: ip,
                           IGNORED: 0}

        # VOLTAGE
        try:
            powerplant_data[OUTPUT_VOLTAGE] = round(float(consulta_snmp(ip, Vo, snmp_ver=1, community='public')[1] / 1000), 2)
        except:
            pass

        # CURRENTS
        try:
            powerplant_data[RECTIFIER_OUTPUT_CURRENT] = round(float(consulta_snmp(ip, Io, snmp_ver=1, community='public')[1] / 1000), 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_CURRENT] = round(float(consulta_snmp(ip, Ib, snmp_ver=1, community='public')[1]), 2)
        except:
            pass


        try:
            powerplant_data[LOAD_OUTPUT_CURRENT] = round(float(consulta_snmp(ip, Il, snmp_ver=1, community='public')[1] / 1000), 2)
        except:
            pass

        # POWER
        try:
            powerplant_data[RECTIFIER_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                            powerplant_data[RECTIFIER_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[BATTERY_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                          powerplant_data[BATTERY_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            powerplant_data[LOAD_OUTPUT_POWER] = round(powerplant_data[OUTPUT_VOLTAGE] *
                                                       powerplant_data[LOAD_OUTPUT_CURRENT] / 1000, 2)
        except:
            pass

        try:
            irc = int(consulta_snmp(ip, Irc, snmp_ver=1, community='public')[1])
            powerplant_data[OUTPUT_POWER] = round(3000 * irc / 1000, 2)
        except:
            pass

    return powerplant_data


# COMMERCIAL ENERGY
def commercial_energy_ion(ip, device_name):
    """
    ION Commercial Energy SNMP collector
    :param ip: string. Commercial Energy IP address
    :param device_name: string. Commercial Energy name
    :return: dic/None. commercial_energy_data
    """
    if check_connectivity(ip) == 'NOK':
        data = None

    else:
        Vlna = '1.3.6.1.4.1.10439.1.0'
        Vlnb = '1.3.6.1.4.1.10439.2.0'
        Vlnc = '1.3.6.1.4.1.10439.3.0'

        Ia = '1.3.6.1.4.1.10439.10.0'
        Ib = '1.3.6.1.4.1.10439.11.0'
        Ic = '1.3.6.1.4.1.10439.12.0'

        Pf = '1.3.6.1.4.1.10439.19.0'
        PkW = '1.3.6.1.4.1.10439.20.0'
        PkVA = '1.3.6.1.4.1.10439.22.0'

        data = {COMMERCIAL_ENERGY_NAME: device_name,
                DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                IP_ADDRESS: ip,
                IGNORED: 0}

        # VOLTAGE
        try:
            data[OUTPUT_VOLTAGE_L1] = round(float(consulta_snmp(ip, Vlna)[1].prettyPrint()), 2)
        except:
            pass

        try:
            data[OUTPUT_VOLTAGE_L2] = round(float(consulta_snmp(ip, Vlnb)[1].prettyPrint()), 2)
        except:
            pass

        try:
            data[OUTPUT_VOLTAGE_L3] = round(float(consulta_snmp(ip, Vlnc)[1].prettyPrint()), 2)
        except:
            pass


        # CURRENT
        try:
            data[OUTPUT_CURRENT_L1] = round(float(consulta_snmp(ip, Ia)[1].prettyPrint()), 2)
        except:
            pass

        try:
            data[OUTPUT_CURRENT_L2] = round(float(consulta_snmp(ip, Ib)[1].prettyPrint()), 2)
        except:
            pass

        try:
            data[OUTPUT_CURRENT_L3] = round(float(consulta_snmp(ip, Ic)[1].prettyPrint()), 2)
        except:
            pass


        # REAL POWER
        try:
            data[REAL_OUTPUT_POWER] = round(float(consulta_snmp(ip, PkW)[1].prettyPrint()), 2)
        except:
            pass


        # APPARENT POWER
        try:
            data[APPARENT_OUTPUT_POWER] = round(float(consulta_snmp(ip, PkVA)[1].prettyPrint()), 2)
        except:
            pass


        # POWER FACTOR
        try:
            data[POWER_FACTOR] = round(float(consulta_snmp(ip, Pf)[1].prettyPrint())/100, 3)
        except:
            pass

    return data


def commercial_energy_sch_pm800(ip, device_name):
    """
    SCHNEIDER PM800 Series ModbusTCP collector
    :param ip: string. Commercial Energy IP address
    :param device_name: string. Commercial Energy name
    :return: dic/None. commercial_energy_data
    """
    if check_connectivity(ip) == 'NOK':
        data = None

    else:
        # Voltage A-N, B-N, C-N
        Va, Vb, Vc = 1123, 1124, 1125

        # Current Phase A, B, C
        Ia, Ib, Ic = 1099, 1100, 1101

        # Real power, apparent power and power factor
        Rp, Ap, Pf = 1142, 1150, 1162

        data = {COMMERCIAL_ENERGY_NAME: device_name,
                DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                IP_ADDRESS: ip,
                IGNORED: 0}

        # VOLTAGE
        try:
            data[OUTPUT_VOLTAGE_L1] = consulta_modbustcp(ip, Va)
        except:
            pass

        try:
            data[OUTPUT_VOLTAGE_L2] = consulta_modbustcp(ip, Vb)
        except:
            pass

        try:
            data[OUTPUT_VOLTAGE_L3] = consulta_modbustcp(ip, Vc)
        except:
            pass


        # CURRENT
        try:
            data[OUTPUT_CURRENT_L1] = consulta_modbustcp(ip, Ia)
        except:
            pass

        try:
            data[OUTPUT_CURRENT_L2] = consulta_modbustcp(ip, Ib)
        except:
            pass

        try:
            data[OUTPUT_CURRENT_L3] = consulta_modbustcp(ip, Ic)
        except:
            pass


        # REAL POWER
        try:
            data[REAL_OUTPUT_POWER] = consulta_modbustcp('10.105.192.73', Rp) - 65536
        except:
            pass


        # APPARENT POWER
        try:
            data[APPARENT_OUTPUT_POWER] = consulta_modbustcp('10.105.192.73', Ap)
        except:
            pass


        # POWER FACTOR
        try:
            data[POWER_FACTOR] = round(consulta_modbustcp('10.105.192.73', Pf)/1000, 3)
        except:
            pass

    return  data


# POWERPLANT BREAKERS
def powerplant_breakers(ip, device_name):
    """
    PowerPlant Breakers SNMP collector
    :param device_name: string. PowerPlant name
    :param ip: string. UPS IP address
    :return: dic/None. breaker_data
    """
    if check_connectivity(ip) == 'NOK':
        breakers_data = None

    else:
        # POWERPLANT BREAKERS OIDs
        # Current values
        val_c110 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.105'
        val_c210 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.106'
        val_c310 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.107'
        val_c410 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.108'
        val_c510 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.109'
        val_c610 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.110'

        val_c120 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.217'
        val_c220 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.218'
        val_c320 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.219'
        val_c420 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.220'
        val_c520 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.221'
        val_c620 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.222'

        val_c130 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.329'
        val_c230 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.330'
        val_c330 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.331'
        val_c430 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.332'
        val_c530 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.333'
        val_c630 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.334'

        val_c140 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.441'
        val_c240 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.442'
        val_c340 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.443'
        val_c440 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.444'
        val_c540 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.445'
        val_c640 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.446'

        val_c150 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.553'
        val_c250 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.554'
        val_c350 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.555'
        val_c450 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.556'
        val_c550 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.557'
        val_c650 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.558'

        val_c160 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.665'
        val_c260 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.666'
        val_c360 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.667'
        val_c460 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.668'
        val_c560 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.669'
        val_c660 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.7.670'

        # Descriptions
        des_c110 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.105'
        des_c210 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.106'
        des_c310 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.107'
        des_c410 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.108'
        des_c510 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.109'
        des_c610 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.110'

        des_c120 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.217'
        des_c220 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.218'
        des_c320 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.219'
        des_c420 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.220'
        des_c520 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.221'
        des_c620 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.222'

        des_c130 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.329'
        des_c230 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.330'
        des_c330 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.331'
        des_c430 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.332'
        des_c530 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.333'
        des_c630 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.334'

        des_c140 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.441'
        des_c240 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.442'
        des_c340 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.443'
        des_c440 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.444'
        des_c540 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.445'
        des_c640 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.446'

        des_c150 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.553'
        des_c250 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.554'
        des_c350 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.555'
        des_c450 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.556'
        des_c550 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.557'
        des_c650 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.558'

        des_c160 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.665'
        des_c260 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.666'
        des_c360 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.667'
        des_c460 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.668'
        des_c560 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.669'
        des_c660 = '1.3.6.1.4.1.1751.2.71.1.1.46.1.3.670'

        ids = ['C110', 'C210', 'C310', 'C410', 'C510', 'C610',
               'C120', 'C220', 'C320', 'C420', 'C520', 'C620',
               'C130', 'C230', 'C330', 'C430', 'C530', 'C630',
               'C140', 'C240', 'C340', 'C440', 'C540', 'C640',
               'C150', 'C250', 'C350', 'C450', 'C550', 'C650',
               'C160', 'C260', 'C360', 'C460', 'C560', 'C660']

        vals = [val_c110, val_c210, val_c310, val_c410, val_c510, val_c610,
                val_c120, val_c220, val_c320, val_c420, val_c520, val_c620,
                val_c130, val_c230, val_c330, val_c430, val_c530, val_c630,
                val_c140, val_c240, val_c340, val_c440, val_c540, val_c640,
                val_c150, val_c250, val_c350, val_c450, val_c550, val_c650,
                val_c160, val_c260, val_c360, val_c460, val_c560, val_c660]

        desc = [des_c110, des_c210, des_c310, des_c410, des_c510, des_c610,
                des_c120, des_c220, des_c320, des_c420, des_c520, des_c620,
                des_c130, des_c230, des_c330, des_c430, des_c530, des_c630,
                des_c140, des_c240, des_c340, des_c440, des_c540, des_c640,
                des_c150, des_c250, des_c350, des_c450, des_c550, des_c650,
                des_c160, des_c260, des_c360, des_c460, des_c560, des_c660]

        breakers_data = []
        for i in range(len(ids)):

            data = {BREAKER_NAME: device_name + '_' + ids[i],
                    DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                    IGNORED: 0}

            try:
                data[MEASURED_CURRENT] = float(consulta_snmp(ip, vals[i], snmp_ver=1)[1][0:-2])
            except:
                data[MEASURED_CURRENT] = None

            try:
                data[DESCRIPTION] = str(consulta_snmp(ip, desc[i], snmp_ver=1)[1])
            except:
                data[DESCRIPTION] = None

            if data[MEASURED_CURRENT] != 0 and data[MEASURED_CURRENT] is not None:
                breakers_data.append(data)

    return breakers_data


# COMAP
def comap(ip, device_name, type):
    """
    Commercial Energy / Generator ComAp controller collector
    :param device_name: string. Device name.
    :param ip: string. IP address
    :param type: string. 'gen' for generator, 'ce' for commercial energy
    :return: dictionary.
    """
    measure_data = None
    if check_connectivity(ip) == 'OK':

        try:
            results = snmp_walk(ip, '1.3.6.1.4.1.28634.6')
        except:
            return measure_data

        # Generator Output Voltage per line
        gv1, gv2, gv3 = results[1], results[2], results[3]

        # Main Output Voltage per line
        mv1, mv2, mv3 = results[4], results[5], results[6]

        # Load Currents per line
        lc1, lc2, lc3 = results[7], results[8], results[9]

        # Load real power, load apparent power, power factor
        lrp, lap, pf = results[33], results[34], round(float(results[12]) / 100, 2)

        measure_data = {DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                        IP_ADDRESS: ip,
                        IGNORED: 0,
                        OUTPUT_CURRENT_L1: lc1,
                        OUTPUT_CURRENT_L2: lc2,
                        OUTPUT_CURRENT_L3: lc3,
                        REAL_OUTPUT_POWER: lrp,
                        APPARENT_OUTPUT_POWER: lap,
                        POWER_FACTOR: pf}

        # GENERATOR MEASURES
        if type == 'gen':
            measure_data[GENERATOR_NAME] = device_name
            measure_data[OUTPUT_VOLTAGE_L1] = gv1
            measure_data[OUTPUT_VOLTAGE_L2] = gv2
            measure_data[OUTPUT_VOLTAGE_L3] = gv3

        elif type == 'ce':
            measure_data[COMMERCIAL_ENERGY_NAME] = device_name
            measure_data[OUTPUT_VOLTAGE_L1] = mv1
            measure_data[OUTPUT_VOLTAGE_L2] = mv2
            measure_data[OUTPUT_VOLTAGE_L3] = mv3

    return measure_data


# DEIF (Generador & Commercial Energy)
def deif(ip, device_name, type='ce'):
    """
    DEIF Commercial Energy ModbusTCP collector
    :param ip: string. Commercial Energy IP address
    :param device_name: string. Commercial Energy name
    :return: dic/None. commercial_energy_data
    """
    if check_connectivity(ip) == 'NOK':
        deif_data = None

    else:
        # DEIFs read_registers
        # Main Voltage (L1-L2):    0
        # Main Voltage (L2-L3):    1
        # Main Voltage (L3-L1):    2
        # Main Voltage (L1-N):     3
        # Main Voltage (L2-N):     4
        # Main Voltage (L3-N):     5
        # Main Current (L1):       7
        # Main Current (L2):       8
        # Main Current (L3):       9
        # Main Power:              10
        # Main Reactive Power:     11
        # Main apparent Power:     12
        # Power Factor:            13

        if type == 'ce':
            deif_data = {COMMERCIAL_ENERGY_NAME: device_name}

        elif type == 'gen':
            deif_data = {GENERATOR_NAME: device_name}

        deif_data.update({DATE: time.strftime("%Y-%m-%d %H:%M:%S"),
                          IP_ADDRESS: ip,
                          IGNORED: 0}
                         )

        # VOLTAGE
        try:
            deif_data[OUTPUT_VOLTAGE_L1] = int(consulta_modbustcp(ip, 3, 1))
        except:
            pass

        try:
            deif_data[OUTPUT_VOLTAGE_L2] = int(consulta_modbustcp(ip, 4, 1))
        except:
            pass

        try:
            deif_data[OUTPUT_VOLTAGE_L3] = int(consulta_modbustcp(ip, 5, 1))
        except:
            pass

        # CURRENT
        try:
            deif_data[OUTPUT_CURRENT_L1] = int(consulta_modbustcp(ip, 7, 1))
        except:
            pass

        try:
            deif_data[OUTPUT_CURRENT_L2] = int(consulta_modbustcp(ip, 8, 1))
        except:
            pass

        try:
            deif_data[OUTPUT_CURRENT_L3] = int(consulta_modbustcp(ip, 9, 1))
        except:
            pass

        # REAL POWER
        try:
            deif_data[REAL_OUTPUT_POWER] = float(consulta_modbustcp(ip, 10, 1))
        except:
            pass

        # APPARENT POWER
        try:
            deif_data[APPARENT_OUTPUT_POWER] = float(consulta_modbustcp(ip, 12, 1))
        except:
            pass

        # POWER FACTOR
        try:
            deif_data[POWER_FACTOR] = round(float(consulta_modbustcp(ip, 13, 1) / 100), 3)
        except:
            pass

        # Bug fix: REAL POWER 65000 ??
        try:
            if deif_data[REAL_OUTPUT_POWER] > 50000:
                deif_data[REAL_OUTPUT_POWER] = ''
        except:
            pass

    return deif_data
