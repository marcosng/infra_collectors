import openpyxl


def get_powerplants(file_name):
    """
    Reads the source file and returns lists with PowerPlants information
    :param file_name: string. Source file name.
    :return: *pp_name: list. PowerPlants name
             *pp_brand: list. PowerPlants brand
             *pp_ip: list. PowerPlants IP address
    """
    wb = openpyxl.load_workbook(file_name)
    ws = wb['POWERPLANT']
    pp_name, pp_ip, pp_brand, pp_bmon = [], [], [], []

    for i in range(2, ws.max_row + 1):
        pp_name.append(ws.cell(row=i, column=2).value)
        pp_brand.append(ws.cell(row=i, column=3).value)
        pp_ip.append(ws.cell(row=i, column=5).value)
        pp_bmon.append(ws.cell(row=i, column=6).value)

    return pp_name, pp_ip, pp_brand, pp_bmon


def get_ups(file_name):
    """
    Reads the source file and returns lists with UPSs information
    :param file_name: string. Source file name.
    :return: *ups_name: list. UPSs names
             *ups_brand: list. UPSs brand
             *ups_moedl: list. UPSs model
             *ups_ip: list. UPSs IP address
    """
    wb = openpyxl.load_workbook(file_name)
    ws = wb['UPS']
    ups_name, ups_brand, ups_model, ups_ip = [], [], [], []

    for i in range(2, ws.max_row + 1):
        ups_name.append(ws.cell(row=i, column=2).value)
        ups_brand.append(ws.cell(row=i, column=3).value)
        ups_model.append(ws.cell(row=i, column=4).value)
        ups_ip.append(ws.cell(row=i, column=5).value)

    return ups_name, ups_brand, ups_model, ups_ip


def get_generators(file_name):
    """
    Reads the source file and returns lists with the Generators information
    :param file_name: string. Source file name.
    :return: *gen_name: list. Generators names
             *gen_id: list. Generators ID
             *gen_ip: list. Generators IP address
    """
    wb = openpyxl.load_workbook(file_name)
    ws = wb['GENERATOR']
    gen_names, gen_contr, gen_ips = [], [], []

    for i in range(2, ws.max_row + 1):
        gen_names.append(ws.cell(row=i, column=2).value)
        gen_contr.append(ws.cell(row=i, column=3).value)
        gen_ips.append(ws.cell(row=i, column=4).value)

    return gen_names, gen_contr, gen_ips


def get_commercial_energy(file_name):
    """
    Reads the source file and returns lists with Commercial Energy information
    :param file_name: string. Source file name.
    :return:
    """
    wb = openpyxl.load_workbook(file_name)
    ws = wb['COMMERCIAL_ENERGY']
    ce_names, ce_contr, ce_ips = [], [], []

    for i in range(2, ws.max_row + 1):
        ce_names.append(ws.cell(row=i, column=2).value)
        ce_contr.append(ws.cell(row=i, column=3).value)
        ce_ips.append(ws.cell(row=i, column=4).value)

    return ce_names, ce_contr, ce_ips
