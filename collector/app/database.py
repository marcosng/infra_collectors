from pymongo import MongoClient


class MongoDatabaseManager:

    def __init__(self, database_name):
        self.client = MongoClient('mongodb', 27017, username='mongo', password='mongo')
        self.db = self.client[database_name]

    def show_collections(self):
        """
        Prints current database's collections
        :return:
        """
        print(self.db.collection_names())

    def create_collection(self, collection_name):
        """
        Creates new collection
        :param collection_name: string. Name of the new collection
        :return:
        """
        self.db.create_collection(collection_name)

    def show_collections_data(self, collection=""):
        """
        Prints collections data.
        :param collection: name of the collection which data will be printed.
        If none collection is selected, all collections data will be printed.
        :return:
        """
        if collection == "":
            collections = self.db.collection_names()
        elif collection in self.db.collection_names():
            collections = [collection]

        for collection in collections:
            print(collection)
            for item in self.db[collection].find():
                print(item)

    def insert_data(self, collection_name, data):
        """
        Inserts data (one row) in desired collection
        :param collection_name: string. Collection in which data will be inserted
        :param data: dictionary.
        :return:
        """
        if collection_name in self.db.collection_names():
            self.db[collection_name].insert_one(data)
        else:
            print('ERROR: "{}" collection does not exist!'.format(collection_name))

    def insert_many_data(self, collection_name, data):
        """
        Inserts data (multiple rows) in desired collection
        :param collection_name: string. Collection in which data will be inserted
        :param data: list of dictionaries.
        :return:
        """
        if collection_name in self.db.collection_names():
            self.db[collection_name].insert_many(data)
        else:
            print('ERROR: "{}" collection does not exist!'.format(collection_name))
