#!/usr/bin/env bash

# Builds images and deploys application. DO NOT RUN WITH VPN
docker-compose --file docker-compose.yml --project-name collector up --build