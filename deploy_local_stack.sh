#!/usr/bin/env bash

# Builds images
docker build -t collector -f collector/Dockerfile .
docker build -t extractor -f extractor/Dockerfile .
docker pull sayri/mongo:latest

# Deploys stack
docker stack deploy -c docker-compose.yml collector
