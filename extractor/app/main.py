import os
from flask import Flask, render_template, request, make_response
from wtforms import Form, TextField, validators, SelectField


# App config.
DEBUG = True
app = Flask(__name__)


class Data(Form):
    colection = SelectField('colection:', validators=[validators.required()])
    date = TextField('date:', validators=[validators.required()])
    time = TextField('time:', validators=[validators.required()])


@app.route("/", methods=['GET', 'POST'])
def Form():
    form = Data(request.form)

    if request.method == 'POST':
        colection = request.form['colection']
        date = request.form['date']
        time = request.form['time']

        if date == "" or date is None:
            date = "1900-01-01"
        if time == "" or time is None:
            time = "00:00:01"

        # mongoexport datetime query
        query = '{{Fecha: {{$gt: "{}" }} }}'.format(str(date + " " + time))

        # Columns
        cols = {
            'powerplant': 'Planta de energía cc,Fecha,Dirección IP de adminitración,Potencia de salida instalada medida (kW),Voltaje de salida medido (V),Corriente de salida del rectificador medida (A),Corriente de salida medida de batería (A),Corriente de salida de carga medida (A),Potencia de salida del rectificador medida (kW),Potencia de salida de baterías medida (kW),Potencia de salida de carga medida (kW),Ignorado',
            'ups': 'Ups,Fecha,Dirección IP de adminitración,Potencia de salida instalada medida (kW),Voltaje de salida medido en línea 1 (V),Voltaje de salida medido en línea 2 (V),Voltaje de salida medido en línea 3 (V),Corriente de salida medida en línea 1 (A),Corriente de salida medida en línea 2 (A),Corriente de salida medida en línea 3 (A),Potencia real de salida (kW),Potencia aparente de salida (KVA),Factor de potencia,Ignorado',
            'generator': 'Generador,Fecha,Dirección IP de adminitración,Voltaje de salida medido en línea 1 (V),Voltaje de salida medido en línea 2 (V),Voltaje de salida medido en línea 3 (V),Corriente de salida medida en línea 1 (A),Corriente de salida medida en línea 2 (A),Corriente de salida medida en línea 3 (A),Potencia real de salida (kW),Potencia aparente de salida (KVA),Factor de potencia,Ignorado',
            'commercial_energy': 'Energía comercial,Fecha,Dirección IP de adminitración,Voltaje de salida medido en línea 1 (V),Voltaje de salida medido en línea 2 (V),Voltaje de salida medido en línea 3 (V),Corriente de salida medida en línea 1 (A),Corriente de salida medida en línea 2 (A),Corriente de salida medida en línea 3 (A),Potencia real de salida (kW),Potencia aparente de salida (KVA),Factor de potencia,Ignorado',
            'powerplant_breakers': 'Interruptor,Fecha,Corriente medida (A),Descripcion,Ignorado',
        }

        # Running in terminal
        command = "mongoexport --username mongo --password mongo --authenticationDatabase admin --host mongodb -d collector -c {} -q '{}' -f '{}' --type csv".format(
            colection, query, cols[colection])
        csv = os.popen(command).read()
        #print(csv)

        if csv != "" and csv is not None:
            response = make_response(csv)
            cd = 'attachment; filename={}_{}_{}.csv'.format(colection, date, time.replace(":", ""))
            response.headers['Content-Disposition'] = cd
            response.mimetype = 'text/csv'

            return response
        else:
            return render_template('error.html')

    return render_template('form.html', form=form)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)
